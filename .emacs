;;; package --- Summary:
;;; Emacs-Config
;;; Simon Braß <brass@physik.uni-siegen.de>

;;; Commentary:
;;; There is no need to directly load the package system of Emacs.
;;; Cask and pallet do everything necessarily.

;;; Code:
(require 'cask "~/.cask/cask.el")
(cask-initialize)

(require 'pallet)
(pallet-mode t)

;; Change the path
(setenv "PATH" (concat "/home/simon/.local/bin:" (getenv "PATH")))
(setq exec-path (append '("/home/simon/.local/bin") exec-path))


;; add your modules path
(add-to-list 'load-path "~/.emacs.d/custom/")

;; load your modules
(require 'setup-applications)
(require 'setup-communication)
(require 'setup-convenience)
(require 'setup-data)
(require 'setup-development)
(require 'setup-editing)
(require 'setup-environment)
(require 'setup-external)
(require 'setup-faces-and-ui)
(require 'setup-files)
(require 'setup-helm)
(require 'setup-help)
(require 'setup-programming)
(require 'setup-text)
;; (require 'setup-local)

(defalias 'yes-or-no-p 'y-or-n-p)	; y or n is enough
(defalias 'list-buffers 'ibuffer) ; always use ibuffer

                                        ; elisp
(defalias 'eb 'eval-buffer)
(defalias 'er 'eval-region)
(defalias 'ed 'eval-defun)

                                        ; minor modes
(defalias 'wsm 'whitespace-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-command-style
   (quote
    (("" "%(PDF)%(latex) -shell-escape %(file-line-error) %(extraopts) %S%(PDFout)"))))
 '(LaTeX-electric-left-right-brace t)
 '(TeX-electric-escape t)
 '(TeX-electric-math (quote ("$" . "$")))
 '(TeX-electric-sub-and-superscript t)
 '(TeX-shell-command-option "-c")
 '(custom-enabled-themes (quote (monokai)))
 '(custom-safe-themes
   (quote
    ("38ba6a938d67a452aeb1dada9d7cdeca4d9f18114e9fc8ed2b972573138d4664" "0fb6369323495c40b31820ec59167ac4c40773c3b952c264dd8651a3b704f6b5" "196cc00960232cfc7e74f4e95a94a5977cb16fd28ba7282195338f68c84058ec" "05c3bc4eb1219953a4f182e10de1f7466d28987f48d647c01f1f0037ff35ab9a" default)))
 '(flycheck-disabled-checkers (quote (tex-chktex)))
 '(org-babel-post-tangle-hook (quote (indent-region-or-buffer)))
 '(org-latex-listings (quote minted))
 '(org-latex-minted-langs
   (quote
    ((emacs-lisp "common-lisp")
     (cc "c++")
     (cperl "perl")
     (shell-script "bash")
     (caml "ocaml")
     (f90 "fortran"))))
 '(org-latex-packages-alist (quote (("" "minted" t))))
 '(org-latex-pdf-process
   (quote
    ("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f" "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f" "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f")))
 '(org-src-preserve-indentation t)
 '(package-selected-packages
   (quote
    (auctex f async company web-mode fortpy package-build yasnippet git-commit helm-core magit-popup with-editor flycheck helm ess workgroups2 volatile-highlights vlf undo-tree smartparens shell-pop recentf-ext rebox2 rainbow-mode projectile pkgbuild-mode php-mode pallet nyan-mode monokai-theme magit info+ highlight-symbol highlight-numbers help-mode+ help-fns+ help+ helm-spotify helm-descbinds golden-ratio flycheck-tip expand-region esup duplicate-thing discover-my-major dired+ diff-hl company-auctex clean-aindent-mode auto-complete)))
 '(safe-local-variable-values
   (quote
    ((ess-noweb-default-code-modework . f90-mode)
     (noweb-default-code-mode . f90-mode)
     (ess-noweb-default-code-mode . f90-mode))))
 '(save-place t nil (saveplace))
 '(save-place-mode t nil (saveplace)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
