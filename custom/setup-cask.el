(source gnu)
(source melpa)

(depends-on "anaphora")
(depends-on "async")
(depends-on "auctex")
(depends-on "auto-complete")
(depends-on "clean-aindent-mode")
(depends-on "company")
(depends-on "company-auctex")
(depends-on "diff-hl")
(depends-on "dired+")
(depends-on "discover-my-major")
(depends-on "duplicate-thing")
(depends-on "expand-region")
(depends-on "flycheck")
(depends-on "flycheck-tip")
(depends-on "git-commit")
(depends-on "golden-ratio")
(depends-on "helm")
(depends-on "helm-core")
(depends-on "helm-descbinds")
(depends-on "helm-spotify")
(depends-on "help+")
(depends-on "help-fns+")
(depends-on "help-mode+")
(depends-on "highlight-numbers")
(depends-on "highlight-symbol")
(depends-on "info+")
(depends-on "magit")
(depends-on "magit-popup")
(depends-on "monokai-theme")
(depends-on "nyan-mode")
(depends-on "parent-mode")
(depends-on "pkg-info")
(depends-on "popup")
(depends-on "projectile")
(depends-on "rainbow-mode")
(depends-on "rebox2")
(depends-on "recentf-ext")
(depends-on "shell-pop")
(depends-on "smartparens")
(depends-on "undo-tree")
(depends-on "vlf")
(depends-on "volatile-highlights")
(depends-on "workgroups2")
(depends-on "yasnippet")

(provide 'setup-cask)
;;; setup-cask.el ends here
