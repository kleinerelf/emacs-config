Prerequisite
============

Installation of packages (via melpa and package manager)

- auctex
- auto-complete
- preview-latex
- latex-preview-pane

Installation
============

Clone git and link your *.emacs* to the one in the repository.

.. code-block:: bash

   cd ~
   git clone git@bitbucket.org:kleinerelf/emacs-config.git emacs-config
   ln -s emacs-config/.emacs .emacs

Optionally you can comment the load-path commands out and change them to your needs.

Enjoy your nicely configured Emacs!
